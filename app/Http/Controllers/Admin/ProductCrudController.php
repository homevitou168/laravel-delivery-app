<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Product::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/product');
        CRUD::setEntityNameStrings('product', 'products');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
//        CRUD::setFromDb(); // columns

        $this->crud->addColumns([
            [
                'label' => 'Name',
                'name'  => 'name'
            ],
            [
                'label' => "Category",
                'name'  => "parent_id",
                'type'  => "closure",
                'function' => function($entry)
                {
                    return optional(Category::find($entry->category_id))->name;
                }
            ],
            [
                'label' => "Photo",
                'name'  => "main_photo",
                'type'  => "image",
            ],
            [
                'label' => 'Price',
                'name'  => 'price'
            ],


        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        //CRUD::setValidation(ProductRequest::class);

       // CRUD::setFromDb(); // fields

        $this->crud->addFields([
            [
                'label' => 'Name',
                'name' => 'name',
                'type' => 'text'
            ],
            [
                'label' => 'Category',
                'name' => 'category_id',
                'type' => 'select2_from_array',
                'options' => Category::pluck("name","id")->toArray()
            ],
            [   // Upload
                'name'      => 'main_photo',
                'label'     => 'Main Photo',
                'type'      => 'upload',
                'upload'    => true,
                // 'disk'      => 'public', // if you store files in the /public folder, please omit this; if you store them in /storage or S3, please specify it;
                // optional:
                'temporary' => 10 // if using a service, such as S3, that requires you to make temporary URLs this will make a URL that is valid for the number of minutes specified
            ],
            [
                'label' => 'Price',
                'name' => 'price',
                'type' => 'number',
                'attributes' => ["step" => "any"],
            ],
            [
                'label' => 'Description',
                'name' => 'desc',
                'type' => 'ckeditor',
            ],
            [   // Upload
                'name'      => 'other_photos',
                'label'     => 'Other photo',
                'type'      => 'upload_multiple',
                'upload'    => true,
                //'disk'      => 'uploads', // if you store files in the /public folder, please omit this; if you store them in /storage or S3, please specify it;
                // optional:
                'temporary' => 10 // if using a service, such as S3, that requires you to make temporary URLs this will make a URL that is valid for the number of minutes specified
            ],
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
